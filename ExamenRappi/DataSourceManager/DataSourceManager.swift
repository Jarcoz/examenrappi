//
//  DataSourceManager.swift
//  ExamenRappi
//
//  Created by jarcos on 6/1/19.
//  Copyright © 2019 com.alphasoluciones.apps. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import Alamofire
import ObjectMapper

final class DataSourceManager {
    // Get String model of Realm
    static func requestDataFromCache(realName: String, cacheManagerName: String) -> String? {
        var stringValue: String?
        //print("Reading Data from cache")
        
        if let cacheManager = try? RealmCacheManager(realmName: cacheManagerName ) {
            if let cacheObject = cacheManager.get(realName) {
                stringValue = cacheObject.value
            }
        }
        else {
            print("Data NOT read")
        }
        
        return stringValue
    }
    
    // Save String model of Realm
    static func saveCacheModel(_ urlString: String, value: String) -> ManagerResult {
        print("Saving CacheModel to cache")
        if let cacheManager = try? RealmCacheManager(realmName: Constants.KeysRealmObject.CacheManagerRealm) {
            cacheManager.add(urlString, value: value)
            print("CacheModel saved to cache successfully")
            return .success
        }
        else {
            print("CacheModel NOT saved")
            return .failure(message: "Error al guardar Dato en DB")
        }
    }
    
    // Remove Entity from REalm
    static func removeCacheModel(_ urlString: String) -> ManagerResult {
        print("Saving CacheModel to cache")
        if let cacheManager = try? RealmCacheManager(realmName: Constants.KeysRealmObject.CacheManagerRealm) {
            cacheManager.remove(urlString)
            print("CacheModel delete to cache successfully")
            return .success
        }
        else {
            print("CacheModel NOT saved")
            return .failure(message: "Error al guardar Dato en DB")
        }
    }
    
    // Remove All DB
    static func removeAllData() -> ManagerResult {
        print("Remove AllData from DB")
        if let cacheManager = try? RealmCacheManager(realmName: Constants.KeysRealmObject.CacheManagerRealm) {
            cacheManager.removeAll()
            print("CacheModel delete All DB to cache successfully")
            return .success
        }
        else {
            print("CacheModel NOT saved")
            return .failure(message: "Error al guardar Dato en DB")
        }
    }
    
    static func getMoviesByType(typeMovie: TypeMoviesEnum) -> [ResultMovieModel] {
        guard let userDataJson = DataSourceManager.requestDataFromCache(realName: typeMovie.keyRealmValue, cacheManagerName: Constants.KeysRealmObject.CacheManagerRealm) else {
            return [ResultMovieModel]()
        }
        
        guard let model : MoviesListModel  = Mapper<MoviesListModel>().map(JSONString: userDataJson) else {
            return [ResultMovieModel]()
        }
        
        return model.results ?? [ResultMovieModel]()
    }
    
    static func getMovieVideosById(id : String, typeMovie: TypeMoviesEnum) -> VideosModel{
        guard let userDataJson = DataSourceManager.requestDataFromCache(realName: typeMovie.keyRealmValue + "_\(id)", cacheManagerName: Constants.KeysRealmObject.CacheManagerRealm) else {
            return VideosModel()
        }
        
        guard let model : VideosModel = Mapper<VideosModel>().map(JSONString: userDataJson) else {
            return VideosModel()
        }
        
        return model
    }
    
    static func getTvsByType(typeTv: TypeTvsEnum) -> [ResultTvModel] {
        guard let userDataJson = DataSourceManager.requestDataFromCache(realName: typeTv.keyRealmValue, cacheManagerName: Constants.KeysRealmObject.CacheManagerRealm) else {
            return [ResultTvModel]()
        }
        
        guard let model : TvsListModel  = Mapper<TvsListModel>().map(JSONString: userDataJson) else {
            return [ResultTvModel]()
        }
        
        return model.results ?? [ResultTvModel]()
    }
    
    static func getTvVideosById(id : String, typeTv: TypeTvsEnum) -> VideosModel{
        guard let userDataJson = DataSourceManager.requestDataFromCache(realName: typeTv.keyRealmValue + "_\(id)", cacheManagerName: Constants.KeysRealmObject.CacheManagerRealm) else {
            return VideosModel()
        }
        
        guard let model : VideosModel = Mapper<VideosModel>().map(JSONString: userDataJson) else {
            return VideosModel()
        }
        
        return model
    }
    
    static func getVideoTrailerIdByMovieId(id : String, typeMovie: TypeMoviesEnum) -> String{
        
        let model = self.getMovieVideosById(id: id, typeMovie: typeMovie)
        
        if let videos = model.results {
            let video = videos.filter({ $0.type == "Trailer" })
            return video.first?.key ?? ""
        }
        
        return ""
    }
    
    
    static func getVideoTrailerIdByTvId(id : String, typeTv: TypeTvsEnum) -> String{
        
        let model = self.getTvVideosById(id: id, typeTv: typeTv)
        
        if let videos = model.results {
            let video = videos.filter({ $0.type == "Trailer" })
            return video.first?.key ?? ""
        }
        
        return ""
    }
    
}
