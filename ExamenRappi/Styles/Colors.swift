//
//  Colors.swift
//  ExamenRappi
//
//  Created by jarcos on 6/1/19.
//  Copyright © 2019 com.alphasoluciones.apps. All rights reserved.
//

import Foundation
import UIKit

public enum ColorPallete: String {
    
    // Color General
    case white          = "#FFFFFF"
    case black          = "#000000"
    
    case rgb_b8d8d8     = "#b8d8d8"
    case rgb_7a9e9f     = "#7a9e9f"
    case rgb_4f6367     = "#4f6367"
    case rgb_eef5db     = "#eef5db"
    case rgb_fe5f55     = "#fe5f55"
    case rgb_3b3960     = "#3b3960"
    case rgb_e5e5ec     = "#e5e5ec"
    case rgb_595d9f     = "#595d9f"
    
    public  func asColor() -> UIColor {
        return hexStringToUIColor(hex: self.rawValue)
    }
    
    public func asColor(withAlpha: CGFloat) -> UIColor {
        let color = self.asColor()
        return color.withAlphaComponent(withAlpha)
    }
    
    private func hexStringToUIColor (hex: String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
