//
//  CustomSegue.swift
//  ExamenRappi
//
//  Created by jarcos on 6/3/19.
//  Copyright © 2019 com.alphasoluciones.apps. All rights reserved.
//

import Foundation
import UIKit

class BottomCardSegue: UIStoryboardSegue {
    override func perform() {
        source.present(destination, animated: true, completion: nil)
    }
}

protocol SegueHandlerType {
    associatedtype SegueIdentifier: RawRepresentable
}

extension SegueHandlerType where Self: UIViewController, SegueIdentifier.RawValue == String {
    func performSegue(withIdentifier identifier: SegueIdentifier, sender: Any?) {
        performSegue(withIdentifier: identifier.rawValue, sender: sender)
    }
    
    func segueIdentifier(for segue: UIStoryboardSegue) -> SegueIdentifier {
        guard let identifier = segue.identifier,
            let segueIdentifier = SegueIdentifier(rawValue: identifier)
            else {
                fatalError("Invalid segue identifier: \(String(describing: segue.identifier))")
        }
        
        return segueIdentifier
    }
}
