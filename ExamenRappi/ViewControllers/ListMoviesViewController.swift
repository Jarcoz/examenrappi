//
//  ListMoviesViewController.swift
//  ExamenRappi
//
//  Created by jarcos on 6/1/19.
//  Copyright © 2019 com.alphasoluciones.apps. All rights reserved.
//

import UIKit

class ListMoviesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var searchActive : Bool = false
    
    lazy var typeMovie : TypeMoviesEnum = {
        return .Popular
    }()
    
    lazy var typeTv : TypeTvsEnum = {
       return .Popular
    }()
    
    lazy var typeContent : TypeContentList = {
        return .movies
    }()
    
    lazy var contentData : [AnyObject] = {
        return [AnyObject]()
    }()
    
    lazy var filtered : [AnyObject] = {
        return [AnyObject]()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.typeContent == .movies ? typeMovie.title : typeTv.title
        navigationController?.navigationBar.barTintColor = ColorPallete.rgb_3b3960.asColor()
        navigationController?.navigationBar.tintColor = ColorPallete.rgb_e5e5ec.asColor()
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: ColorPallete.rgb_eef5db.asColor()]
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        self.tableView.estimatedRowHeight = 50.0
        self.tableView.rowHeight = UITableView.automaticDimension
        
        self.getContentData()
        
        registerForPreviewing(with: self, sourceView: self.tableView)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    private func getContentData() {
        
        if self.typeContent == .movies {
           self.contentData = DataSourceManager.getMoviesByType(typeMovie: self.typeMovie)
        }else {
           self.contentData = DataSourceManager.getTvsByType(typeTv: self.typeTv)
        }
        
        self.tableView.reloadData()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "segueToMovieDetail" {
            if let destination = segue.destination as? MovieDetailViewController {
                
                if self.typeContent == .movies {
                    if let _result = sender as? ResultMovieModel {
                        destination.dataSel = _result
                    }
                }else {
                    if let _result = sender as? ResultTvModel {
                        destination.dataSel = _result
                    }
                }
                destination.typeContent = self.typeContent
                destination.transitioningDelegate = self
            }
        }
    }
    
    
    func createDetailViewControllerIndexPath(indexPath: IndexPath) -> UIViewController{
        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        guard let vc = storyboard.instantiateViewController(withIdentifier: "MovieDetailViewController") as? MovieDetailViewController else {
            fatalError("Couldn't load detail view controller")
        }
        
        vc.dataSel = contentData[indexPath.row]
        
        return vc
    }
    
    
    
}

extension ListMoviesViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contentData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMovie", for: indexPath)
        
        let viewContent = cell.viewWithTag(10)
        viewContent!.layer.cornerRadius = 10
        
        let title = cell.viewWithTag(100) as! UILabel
        if self.typeContent == .movies {
            title.text = (contentData[indexPath.row] as! ResultMovieModel).original_title
        }else {
            title.text = (contentData[indexPath.row] as! ResultTvModel).original_name
        }
        
        let overview = cell.viewWithTag(200) as! UILabel
        if self.typeContent == .movies {
            overview.text = (contentData[indexPath.row] as! ResultMovieModel).overview
        }else {
            overview.text = (contentData[indexPath.row] as! ResultTvModel).overview
        }
        
        let releaseDate = cell.viewWithTag(300) as! UILabel
        if self.typeContent == .movies {
            releaseDate.text = (contentData[indexPath.row] as! ResultMovieModel).release_date
        }else {
            releaseDate.text = (contentData[indexPath.row] as! ResultTvModel).first_air_date
        }
        
        if let imageCover = cell.viewWithTag(500) as? UIImageView {
            if self.typeContent == .movies {
                imageCover.getImageWithUrl(url:"\(Constants.UrlServices.ImagePath)\((contentData[indexPath.row] as! ResultMovieModel).poster_path ?? "")", cache: true)
            }else {
                imageCover.getImageWithUrl(url:"\(Constants.UrlServices.ImagePath)\((contentData[indexPath.row] as! ResultTvModel).poster_path ?? "")", cache: true)
            }
        }
        

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
//        let animation = AnimationFactory.makeFadeAnimation(duration: 0.5, delayFactor: 0.05)
//        let animator = Animator(animation: animation)
//        animator.animate(cell: cell, at: indexPath, in: tableView)
        
//        let animation = AnimationFactory.makeMoveUpWithBounce(rowHeight: cell.frame.height, duration: 1.0, delayFactor: 0.05)
//        let animator = Animator(animation: animation)
//        animator.animate(cell: cell, at: indexPath, in: tableView)
        
//        let animation = AnimationFactory.makeMoveUpWithFade(rowHeight: cell.frame.height, duration: 0.5, delayFactor: 0.05)
//        let animator = Animator(animation: animation)
//        animator.animate(cell: cell, at: indexPath, in: tableView)
        
        let animation = AnimationFactory.makeSlideIn(duration: 0.5, delayFactor: 0.05)
        let animator = Animator(animation: animation)
        if !searchActive{
            animator.animate(cell: cell, at: indexPath, in: tableView)
        }
    }
}

extension ListMoviesViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "segueToMovieDetail", sender: contentData[indexPath.row])
    }
}

extension ListMoviesViewController : UIViewControllerPreviewingDelegate {
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        guard let indexPath = tableView.indexPathForRow(at: location) else {
            return nil
        }
        
        let detailViewController = createDetailViewControllerIndexPath(indexPath: indexPath)
        
        return detailViewController
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        navigationController?.pushViewController(viewControllerToCommit, animated: true)
    }
    
}

extension ListMoviesViewController: SegueHandlerType {
    enum SegueIdentifier: String {
        case reveal
    }
}


extension ListMoviesViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController)
        -> UIViewControllerAnimatedTransitioning? {
            return FlipPresentAnimationController(originFrame: self.view.frame)
    }
}

extension ListMoviesViewController : UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filtered = contentData.filter({ (modelObj) -> Bool in
            
            if self.typeContent == .movies {
                let tmp : ResultMovieModel = modelObj as! ResultMovieModel
                let range = tmp.title?.range(of: searchText, options: .caseInsensitive)
                return range != nil
            }else {
                let tmp : ResultTvModel = modelObj as! ResultTvModel
                let range = tmp.name?.range(of: searchText, options: .caseInsensitive)
                return range != nil
            }
        })
        if(filtered.count == 0){
            searchActive = false;
            self.getContentData()
        } else {
            searchActive = true;
            contentData.removeAll()
            contentData = filtered
            self.tableView.reloadData()
        }
    }
    
}
