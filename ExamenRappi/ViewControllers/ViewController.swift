//
//  ViewController.swift
//  ExamenRappi
//
//  Created by jarcos on 5/29/19.
//  Copyright © 2019 com.alphasoluciones.apps. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ViewController: BaseViewController {

    
    @IBOutlet weak var choseContentType: UISwitch!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var actionPopular: UIButton!
    @IBOutlet weak var actionUpcoming: UIButton!
    @IBOutlet weak var actionTopRated: UIButton!
    
    lazy var disposeBag : DisposeBag = {
        return DisposeBag()
    }()
    
    lazy var typeContent : TypeContentList = {
        return .movies
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        self.setBinding()
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    private func setBinding(){
        
        self.actionPopular.rx.tap.subscribe({ value
            in
            //self.getListMovies(typeMovie: .Popular)
            let typeClasified : AnyObject = self.typeContent == .movies ? TypeMoviesEnum.Popular as AnyObject : TypeTvsEnum.Popular as AnyObject
            self.getContentList(typeContent: typeClasified, typeList: self.typeContent)
            self.actionPopular.makeAnimation(color: ColorPallete.rgb_b8d8d8.asColor(withAlpha: 0.5))
        }).disposed(by: self.disposeBag)
        
        
        self.actionUpcoming.rx.tap.subscribe({ value
            in
            //self.getListMovies(typeMovie: .Upcoming)
            let typeClasified : AnyObject = TypeMoviesEnum.Upcoming as AnyObject
            self.getContentList(typeContent: typeClasified, typeList: self.typeContent)
            self.actionUpcoming.makeAnimation(color: ColorPallete.rgb_b8d8d8.asColor(withAlpha: 0.5))
        }).disposed(by: self.disposeBag)
        
        self.actionTopRated.rx.tap.subscribe({ value
            in
            let typeClasified : AnyObject = self.typeContent == .movies ? TypeMoviesEnum.TopRated as AnyObject : TypeTvsEnum.TopRated as AnyObject
            self.getContentList(typeContent: typeClasified, typeList: self.typeContent)
            self.actionTopRated.makeAnimation(color: ColorPallete.rgb_b8d8d8.asColor(withAlpha: 0.5))
        }).disposed(by: self.disposeBag)
        
        self.choseContentType.rx
            .controlEvent(.valueChanged)
            .withLatestFrom(self.choseContentType.rx.value)
            .subscribe(onNext : { isOn in
                self.typeContent = isOn ? .movies : .tvs
                self.actionUpcoming.isHidden = !isOn
                self.labelTitle.text = isOn ? "Best Movies List" : "Best Tvss List"
            }).disposed(by:
                self.disposeBag)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueListMovies" {
            if let destination = segue.destination as? ListMoviesViewController {
                //ReqsClientModel
                
                if self.typeContent == .movies {
                    if let _typeMovie = sender as? TypeMoviesEnum {
                        destination.typeMovie = _typeMovie
                    }
                }else {
                    if let _typeTv = sender as? TypeTvsEnum {
                        destination.typeTv = _typeTv
                    }
                }
                destination.typeContent = self.typeContent
            }
        }
    }
    
    
    private func getContentList(typeContent : AnyObject, typeList : TypeContentList){
        
        switch typeList {
        case .movies:
            self.getListMovies(typeMovie: typeContent as! TypeMoviesEnum)
        case .tvs:
            self.getListTvSeries(typeTv: typeContent as! TypeTvsEnum)
        }
        
    }
    
    private func getListMovies(typeMovie: TypeMoviesEnum) {
        self.showLoader()
        ServicesManager.getMoviesByType(typeMovie) { (result) in
            self.hiddenLoader(nil, closure: { (_) in })
            switch result {
            case .success:
                print("success")
                self.performSegue(withIdentifier: "segueListMovies", sender: typeMovie)
            case .failure(let message):
                print("failure: \(message)")
                self.validateMovieLocalData(typeMovie: typeMovie)
            case .failureData(let errorModel):
                print("failureData: \(String(describing: errorModel.errorcode))")
                self.showToastMessage(message: Constants.GlobalMessage.Error.errorMessage, type: .warning, slideFrom: .top)
            }
        }

    }
    
    private func getListTvSeries(typeTv : TypeTvsEnum){
        self.showLoader()
        ServicesManager.getTvsByType(typeTv) { (result) in
            self.hiddenLoader(nil, closure: { (_) in })
            switch result {
            case .success:
                print("success")
                self.performSegue(withIdentifier: "segueListMovies", sender: typeTv)
            case .failure(let message):
                print("failure: \(message)")
                self.validateTvsLocalData(typeTv: typeTv)
            case .failureData(let errorModel):
                print("failureData: \(String(describing: errorModel.errorcode))")
                self.showToastMessage(message: Constants.GlobalMessage.Error.errorMessage, type: .warning, slideFrom: .top)
            }
        }
    }
    
    private func validateMovieLocalData(typeMovie: TypeMoviesEnum){
        let data = DataSourceManager.getMoviesByType(typeMovie: typeMovie)
        if data.count > 0 {
            self.performSegue(withIdentifier: "segueListMovies", sender: typeMovie)
        }else {
            self.showToastMessage(message: Constants.GlobalMessage.Error.errorAvailable, type: .warning, slideFrom: .top)
        }
    }
    
    private func validateTvsLocalData(typeTv: TypeTvsEnum){
        let data = DataSourceManager.getTvsByType(typeTv: typeTv)
        if data.count > 0 {
            self.performSegue(withIdentifier: "segueListMovies", sender: typeTv)
        }else {
            self.showToastMessage(message: Constants.GlobalMessage.Error.errorAvailable, type: .warning, slideFrom: .top)
        }
    }

}

