//
//  SplashViewController.swift
//  ExamenRappi
//
//  Created by jarcos on 6/4/19.
//  Copyright © 2019 com.alphasoluciones.apps. All rights reserved.
//

import UIKit
import SwiftyGif

class SplashViewController: UIViewController {

    
    @IBOutlet weak var contentGif: UIView!
    
    let logoGifImageView = UIImageView(gifImage: UIImage(gifName: "splash.gif"), loopCount: 2)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.isHidden = true
        self.contentGif.addSubview(logoGifImageView)
        logoGifImageView.translatesAutoresizingMaskIntoConstraints = false
        logoGifImageView.topAnchor.constraint(equalTo: self.contentGif.topAnchor).isActive = true
        logoGifImageView.bottomAnchor.constraint(equalTo: self.contentGif.bottomAnchor).isActive = true
        logoGifImageView.leadingAnchor.constraint(equalTo: self.contentGif.leadingAnchor).isActive = true
        logoGifImageView.trailingAnchor.constraint(equalTo: self.contentGif.trailingAnchor).isActive = true
        
        logoGifImageView.delegate = self
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        logoGifImageView.startAnimating()
    }
    
}

extension SplashViewController: SwiftyGifDelegate {
    func gifDidStop(sender: UIImageView) {
        contentGif.isHidden = true
        self.performSegue(withIdentifier: "segueNextView", sender: nil)
    }
}
