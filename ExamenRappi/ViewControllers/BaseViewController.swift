//
//  BaseViewController.swift
//  ExamenRappi
//
//  Created by jarcos on 6/1/19.
//  Copyright © 2019 com.alphasoluciones.apps. All rights reserved.
//

import UIKit
import SwiftMessages
import NVActivityIndicatorView
import RxSwift
import Alamofire

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.cancelAllRequet()
    }
    
    //MESSAGE
    func showToastMessage(message : String, type : Theme, slideFrom : SwiftMessages.PresentationStyle ) {
        SwiftMessages.show {
            let mssgView = MessageView.viewFromNib(layout: .cardView)
            // ... configure the view
            mssgView.configureTheme(type)
            mssgView.configureDropShadow()
            mssgView.configureContent(title: "", body: message)
            mssgView.button?.isHidden = true
            var config = SwiftMessages.Config()
            config.interactiveHide = true
            config.duration = .seconds(seconds: 3.5)
            config.presentationStyle = slideFrom
            
            return mssgView
        }
    }

    //Loader
    func showLoader(_ message: String? = nil, closure: @escaping (Bool) -> Void) {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
        let msgLoader = message ?? ""
        let activityData = ActivityData(message: msgLoader, type: .ballGridPulse)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
            closure(true)
        }
    }
    
    func showLoader(_ message: String? = nil) {
        let msgLoader = message ?? ""
        let activityData = ActivityData(message: msgLoader, type: .ballGridPulse)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
    }
    
    func hiddenLoader(_ message: String?, closure: @escaping (Bool) -> Void) {
        
        if let textMessage = message {
            self.changeTextLoader(textMessage)
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            closure(true)
        }
    }
    
    func changeTextLoader(_ message: String) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
            NVActivityIndicatorPresenter.sharedInstance.setMessage(message)
        }
    }
    
    func cancelAllRequet() {
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
            sessionDataTask.forEach { $0.cancel() }
            uploadData.forEach { $0.cancel() }
            downloadData.forEach { $0.cancel() }
        }
    }
}
