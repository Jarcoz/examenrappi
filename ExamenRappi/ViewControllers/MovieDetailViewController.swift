//
//  MovieDetailViewController.swift
//  ExamenRappi
//
//  Created by jarcos on 6/1/19.
//  Copyright © 2019 com.alphasoluciones.apps. All rights reserved.
//

import UIKit
import AlamofireImage
import YoutubePlayer_in_WKWebView

class MovieDetailViewController: BaseViewController {

    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageMovie: UIImageView!
    @IBOutlet weak var labelOriginalTitle: UILabel!
    @IBOutlet weak var labelNumberVotes: UILabel!
    @IBOutlet weak var labelVoteAverage: UILabel!
    @IBOutlet weak var labelOverview: UILabel!
    @IBOutlet weak var playerView: WKYTPlayerView!
    @IBOutlet weak var imageBackGround: UIImageView!
    
    
//    lazy var movieSel : ResultMovieModel = {
//        return ResultMovieModel()
//    }()
    
    
    var dataSel : AnyObject?
    
    lazy var typeContent : TypeContentList = {
        return .movies
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.typeContent == .movies {
            self.title = (dataSel as! ResultMovieModel).title ?? "Movie"
        }else {
            self.title = (dataSel as! ResultTvModel).name ?? "Tv"
        }
        
        
        self.configView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.typeContent == .movies {
            self.getMovieDetail()
        }else {
            self.getTvDetail()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    private func configView() {
        //self.labelTitle.text = movieSel.title
        
        switch self.typeContent {
        case .movies:
            self.imageMovie.getImageWithUrl(url: "\(Constants.UrlServices.ImagePath)\((dataSel as! ResultMovieModel).backdrop_path ?? "")", cache: true)
            self.labelOriginalTitle.text = (dataSel as! ResultMovieModel).original_title
            self.labelNumberVotes.text = "\((dataSel as! ResultMovieModel).vote_count ?? 0) votes"
            self.labelVoteAverage.text = "Vote average: \((dataSel as! ResultMovieModel).vote_average ?? 0)"
            self.labelOverview.text = (dataSel as! ResultMovieModel).overview ?? ""
            
            self.imageBackGround.getImageWithUrl(url: "\(Constants.UrlServices.ImagePath)\((dataSel as! ResultMovieModel).poster_path ?? "")", cache: true, applyMaskColor: ColorPallete.rgb_595d9f.asColor(withAlpha: 1.0))
        case .tvs:
            self.imageMovie.getImageWithUrl(url: "\(Constants.UrlServices.ImagePath)\((dataSel as! ResultTvModel).backdrop_path ?? "")", cache: true)
            self.labelOriginalTitle.text = (dataSel as! ResultTvModel).original_name
            self.labelNumberVotes.text = "\((dataSel as! ResultTvModel).vote_count ?? 0) votes"
            self.labelVoteAverage.text = "Vote average: \((dataSel as! ResultTvModel).vote_average ?? 0)"
            self.labelOverview.text = (dataSel as! ResultTvModel).overview ?? ""
            
            self.imageBackGround.getImageWithUrl(url: "\(Constants.UrlServices.ImagePath)\((dataSel as! ResultTvModel).poster_path ?? "")", cache: true, applyMaskColor: ColorPallete.rgb_595d9f.asColor(withAlpha: 1.0))
        }
        
        
        
        
    }
    
    private func getMovieDetail(){
        ServicesManager.getMovieById("\((dataSel as! ResultMovieModel).id ?? 0)", type: TypeMoviesEnum.Videos) { (result) in
            switch result {
            case .success:
                print("Show Video")
                self.loadVideo(id: "\( (self.dataSel as! ResultMovieModel).id ?? 0)")
            case .failure(let _):
                print("Hide Video")
            case .failureData(let _):
                print("Hide Video")
            }
        }
    }
    
    private func getTvDetail(){
        ServicesManager.getTvById("\((dataSel as! ResultTvModel).id ?? 0)", type: TypeTvsEnum.Videos) { (result) in
            switch result {
            case .success:
                print("Show Video")
                self.loadVideo(id: "\( (self.dataSel as! ResultTvModel).id ?? 0)")
            case .failure(let _):
                print("Hide Video")
            case .failureData(let _):
                print("Hide Video")
            }
        }
    }
    
    private func loadVideo(id : String){
        
        var youtubeId = ""
        
        if self.typeContent == .movies {
            youtubeId = DataSourceManager.getVideoTrailerIdByMovieId(id: id, typeMovie: TypeMoviesEnum.Videos)
        }else {
            youtubeId = DataSourceManager.getVideoTrailerIdByTvId(id: id, typeTv : TypeTvsEnum.Videos)
        }
        
        if !youtubeId.isEmpty {
            playerView.isHidden = false
            playerView.load(withVideoId: youtubeId)
            playerView.delegate = self
        }
    }
    
}

extension MovieDetailViewController : WKYTPlayerViewDelegate {
    func playerView(_ playerView: WKYTPlayerView, receivedError error: WKYTPlayerError) {
        print("Error")
    }
}

