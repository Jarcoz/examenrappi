//
//  Videos.swift
//  ExamenRappi
//
//  Created by jarcos on 6/2/19.
//  Copyright © 2019 com.alphasoluciones.apps. All rights reserved.
//

import Foundation
import ObjectMapper

public class VideosModel : Mappable, CustomStringConvertible {
    
    var id : Int?
    var results : [VideoModel]?
    
    init() {}
    
    required public init?(map: Map){
        
    }
    
    public func mapping(map: Map) {
        id <- map["id"]
        results <- map["results"]
    }
    
    public var description: String {
        return "VideosModel { id : \(String(describing: id)), results: \(String(describing: results)) }"
    }
    
}


public class VideoModel : Mappable, CustomStringConvertible {
    
    var id : String?
    var iso_639_1 : String?
    var iso_3166_1 : String?
    var key : String?
    var name : String?
    var site : String?
    var size : Int?
    var type : String?
    
    init() {}
    
    required public init?(map: Map){
        
    }
    
    public func mapping(map: Map) {
        id <- map["id"]
        iso_639_1 <- map["iso_639_1"]
        iso_3166_1 <- map["iso_3166_1"]
        key <- map["key"]
        name <- map["name"]
        site <- map["site"]
        size <- map["size"]
        type <- map["type"]
    }
    
    public var description: String {
        return " VideoModel { id : \(id), iso_639_1 : \(iso_639_1), iso_3166_1 : \(iso_3166_1), key : \(key), name : \(name), site : \(site), size : \(size), type : \(type) }"
    }
}
