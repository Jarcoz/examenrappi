//
//  Movies.swift
//  ExamenRappi
//
//  Created by jarcos on 5/30/19.
//  Copyright © 2019 com.alphasoluciones.apps. All rights reserved.
//

import Foundation
import ObjectMapper

public class MoviesListModel : Mappable, CustomStringConvertible {
    
    var page : Int?
    var total_results : Int?
    var total_pages : Int?
    var results : [ResultMovieModel]?
    
    init() {}
    
    required public init?(map: Map){
        
    }
    
    public func mapping(map: Map) {
        page <- map["page"]
        total_results <- map["total_results"]
        total_pages <- map["total_pages"]
        results <- map["results"]
    }
    
    public var description: String {
        return "MoviesListModel : {page : \(page), total_results : \(total_results), total_pages : \(total_pages), results : \(results) }"
    }
}

public enum TypeMoviesEnum : String {
    case Popular
    case TopRated
    case Upcoming
    case Videos
    
    var apiMethods : String {
        switch self {
        case .Popular:
            return Constants.UrlMethods.popular
        case .TopRated:
            return Constants.UrlMethods.topRated
        case .Upcoming:
            return Constants.UrlMethods.upcoming
        case .Videos:
            return Constants.UrlMethods.videos
        }
    }
    
    var keyRealmValue : String {
        switch self {
        case .Popular:
            return Constants.KeysRealmObject.RealmMoviePopular
        case .TopRated:
            return Constants.KeysRealmObject.RealmMovieTopRated
        case .Upcoming:
            return Constants.KeysRealmObject.RealmMovieUpcoming
        case .Videos:
            return Constants.KeysRealmObject.RealmMovieVideoId
        }
    }
    
    var title : String {
        switch self {
        case .Popular:
            return "Popular Movies"
        case .TopRated:
            return "Top Rated Movies"
        case .Upcoming:
            return "Upcoming Movies"
        case .Videos:
            return "Video"
        }
    }
    
}


public struct ErrorModel {
    //var errordata: ErrorResponseModel?
    var errorcode: String?
    
    public init (errorcode : String?) {
        //self.errordata = errordata
        self.errorcode = errorcode
    }
}


public class ErrorResponseModel: Mappable, CustomStringConvertible {
    var message:    String   = ""
    var logid:      Int      = 0
    var detail:     String   = ""
    
    init() { }
    
    
    required public init?(map: Map){
        
    }
    
    public func mapping(map: Map) {
        message <- map["message"]
        logid <- map["logid"]
        detail <- map["detail"]
    }
    
    public var description: String {
        return "ErrorResponseModel: {\(message), \(logid), \(detail)}"
    }
    
}
