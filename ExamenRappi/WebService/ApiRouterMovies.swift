//
//  ApiRouterMovies.swift
//  ExamenRappi
//
//  Created by jarcos on 6/1/19.
//  Copyright © 2019 com.alphasoluciones.apps. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper

enum ApiRouterMovies: URLRequestConvertible {
    
    // content Type
    static let contentType = Constants.UrlServices.Content_Type
    
    
    // Cases ApiRouterGrades
    case getMoviesByType(type : TypeMoviesEnum, apiKey : String)
    case getMovieById(type : TypeMoviesEnum, id : String, apiKey : String)
    
    public func asURLRequest() throws -> URLRequest {
        let contentType = ApiRouterMovies.contentType
        var typeMethod: TypeRequest = .POST
        
        let result: (path: String, body: String?) = {
            switch self {
            case .getMoviesByType(let typeMovie, let apiKey):
                typeMethod = .GET
                return ("movie/\(typeMovie.apiMethods)", "?api_key=\(apiKey)")
            case .getMovieById(let typeMovie, let id, let apiKey):
                typeMethod = .GET
                return ("movie/\(id)/\(typeMovie.apiMethods)","?api_key=\(apiKey)")
            }
        }()
            
        let request = APIUtils.createRequestMyAPI(result.path, content_Type: contentType,
                                                      body: result.body, typeRequest: typeMethod);
        
        return request as URLRequest
    }
    
}
